package kds.algorithms.graph;

import java.util.HashSet;
import java.util.Set;

class Graph {
	private final boolean[][] adj;
	private final Vertex[] vertices;
	
	public Graph(int n) {
		adj = new boolean[n][n];
		vertices = new Vertex[n];
		for (int i = 0; i < n; i++) {
			vertices[i] = Vertex.getInstance(i);
		}
	}
	
	public void createEdge(Vertex v, Vertex w) {
		adj[v.id][w.id] = true;
	}

	public void removeEdge(Vertex v, Vertex w) {
		adj[v.id][w.id] = false;
	}
		
	public Set<Vertex> neighbors(Vertex v) {
		Set<Vertex> res = new HashSet<Vertex>();
		for (int i = 0; i < adj.length; i++) {
			if (adj[v.id][i]) {
				res.add(vertices[i]);
			}
		}
		return res;
	}
	
	public Vertex[] vertices() {
		return vertices.clone();
	}
	
	public int numEdgesIn(Vertex w) {
		int res = 0;
		for (int i = 0; i < adj.length; i++) {
			if (adj[i][w.id]) {
				res++;
			}
		}
		return res;
	}

	public int numEdges() {
		int res = 0;
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj.length; j++) {
				if (adj[i][j]) {
					res++;
				}
			}
		}
		return res;
	}
	
	public int numVertices() {
		return adj.length;
	}
}