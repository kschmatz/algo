import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolverTest {
    private int[] p1 = { 0, 10, 19, 28 };
    private int[] p2 = { 0, 9, 20, 30 };

    @Test
    public void testRecursiveCutRod() {
        assertEquals(30, new RecursiveSolver(p1).cutRod(3));
        assertEquals(30, new RecursiveSolver(p2).cutRod(3));
    }
    
    @Test
    public void testMemoizingCutRod() {
        assertEquals(30, new MemoizingSolver(p1).cutRod(3));
        assertEquals(30, new MemoizingSolver(p2).cutRod(3));
    }

    @Test
    public void testBottomUpCutRod() {
        assertEquals(30, new BottomUpSolver(p1).cutRod(3));
        assertEquals(30, new BottomUpSolver(p2).cutRod(3));
    }
}
