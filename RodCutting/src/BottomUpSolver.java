public class BottomUpSolver extends Solver {

    public BottomUpSolver(int[] price) {
        super(price);
    }
    
    @Override
    public int cutRod(int n) {
        int[] r = new int[n + 1];
        for (int j = 1; j <= n; j++) {
            for (int i = 1; i <= j; i++) {
                r[j] = Math.max(r[j], price(i) + r[j - i]);
            }
        }
        return r[n];
    }
}
