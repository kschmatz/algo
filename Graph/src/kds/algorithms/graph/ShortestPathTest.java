package kds.algorithms.graph;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShortestPathTest {

	@Test
	public void testCompute() {
		Graph g = new Graph(6);
		g.createEdge(0, 1, 7);
		g.createEdge(0, 2, 9);
		g.createEdge(0, 5, 14);
		g.createEdge(1, 2, 10);
		g.createEdge(1, 3, 15);
		g.createEdge(2, 3, 11);
		g.createEdge(2, 5, 2);
		g.createEdge(3, 4, 6);
		g.createEdge(4, 5, 9);
		
		ShortestPath dijkstra = new ShortestPath();
		dijkstra.compute(g, 0);

		assertEquals(0, g.getVertex(0).getDistance());
		assertEquals(7, g.getVertex(1).getDistance());
		assertEquals(9, g.getVertex(2).getDistance());
		assertEquals(20, g.getVertex(3).getDistance());
		assertEquals(20, g.getVertex(4).getDistance());
		assertEquals(11, g.getVertex(5).getDistance());
	}
}
