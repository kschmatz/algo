package kds.algorithms.graph;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Graph {
	private final int[][] adj;
	private final Vertex[] vertices;
	
	public Graph(int n) {
		adj = new int[n][n];
		vertices = new Vertex[n];
		for (int i = 0; i < n; i++) {
			Arrays.fill(adj[i], Integer.MAX_VALUE);
			adj[i][i] = 0;
			vertices[i] = new Vertex(i);
		}
	}
	
	public void createEdge(int a, int b, int w) {
		adj[a][b] = w;
		adj[b][a] = w;
	}
	
	public Set<Vertex> getVertices() {
		return new HashSet<Vertex>(Arrays.asList(vertices));
	}

	public int getDistance(Vertex current, Vertex v) {
		return adj[current.id()][v.id()];
	}
	
	public Set<Vertex> getNeighbors(Vertex v) {
		final int a = v.id();
		Set<Vertex> neighbors = new HashSet<Vertex>();
		for (int i = 0; i < adj.length; i++) {
			if (adj[a][i] != 0 && adj[a][i] != Integer.MAX_VALUE) {
				neighbors.add(getVertex(i));
			}
		}
		return neighbors;
	}

	public int size() {
		return adj.length;
	}

	public Vertex getVertex(int id) {
		return vertices[id];
	}
}