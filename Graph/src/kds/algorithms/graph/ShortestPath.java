package kds.algorithms.graph;

import java.util.Collections;
import java.util.Set;

public class ShortestPath {
		
	public void compute(Graph g, int start) {	
		g.getVertex(start).updateDistanceIfShorter(0);
		Set<Vertex> unvisited = g.getVertices();
		
		while (!unvisited.isEmpty()) {
			Vertex current = Collections.min(unvisited);
			unvisited.remove(current);
			for (Vertex v : g.getNeighbors(current)) {
				v.updateDistanceIfShorter(current.getDistance() + g.getDistance(current, v));
			}
		}
	}
}