package kds.algorithms.powering;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class PoweringTest {

	@Test
	public void testPowerRecursive() {
		final BigInteger one = BigInteger.ONE;
		final BigInteger two = BigInteger.valueOf(2);
		final BigInteger ten = BigInteger.TEN;
		for (int n = 0; n < 50; n++) {
			assertEquals(one.pow(n), Powering.powerRecursive(one, n));
			assertEquals(two.pow(n), Powering.powerRecursive(two, n));
			assertEquals(ten.pow(n), Powering.powerRecursive(ten, n));
		}
	}

	@Test
	public void testPowerIterative() {
		final BigInteger one = BigInteger.ONE;
		final BigInteger two = BigInteger.valueOf(2);
		final BigInteger ten = BigInteger.TEN;
		for (int n = 0; n < 50; n++) {
			assertEquals(one.pow(n), Powering.powerIterative(one, n));
			assertEquals(two.pow(n), Powering.powerIterative(two, n));
			assertEquals(ten.pow(n), Powering.powerIterative(ten, n));
		}
	}
}
