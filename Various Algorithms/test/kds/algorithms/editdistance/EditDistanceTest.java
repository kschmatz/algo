package kds.algorithms.editdistance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EditDistanceTest {

	@Test
	public void testDistance() {
		EditDistance dist = new EditDistance();
		assertEquals(6, dist.distance("bc", "abcd"));
		System.out.println(dist.getTransformationSequence());
		assertEquals(54, dist.distance("electrical engineering", "computer science"));
		System.out.println(dist.getTransformationSequence());
	}
}
