package kds.algorithms.skiplist;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

public class SkipListTest {

	private static final int[] TEST_VALUES = { 23, 42, 35, 12, 68, 59, 71, 86 };
	private static final int LOOPS = 1000000;
	
	@Test
	public void testSequence1() {
		SkipList skipList = new SkipList();
		for (int v : TEST_VALUES) {
			skipList.add(v);
			assertTrue(skipList.contains(v));
		}
		for (int v : TEST_VALUES) {
			assertTrue(skipList.remove(v));
			assertFalse(skipList.contains(v));
		}
	}
	
	@Test
	public void testSequence2() {
		Random rand = new Random();
		SkipList skipList = new SkipList();
		int[] values = new int[LOOPS];
		
		for (int i = 0; i < LOOPS; i++) {
			values[i] = rand.nextInt();
			skipList.add(values[i]);
		}
		
//		int[] size = skipList.getSize();
//		for (int s : size) {
//			System.out.print(s + " ");
//		}
//		System.out.println();
		
		for (int val : values) {
			assertTrue(skipList.contains(val));
		}
		for (int val : values) {
			assertTrue(skipList.remove(val));
		}
		for (int val : values) {
			assertFalse(skipList.contains(val));
		}
	}
}
