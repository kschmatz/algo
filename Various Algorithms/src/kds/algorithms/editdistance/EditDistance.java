package kds.algorithms.editdistance;

import java.util.Stack;

public class EditDistance {

	private static class Entry {
		int mCost;
		Transformation mOp;

		public Entry(int cost, Transformation op) {
			mCost = cost;
			mOp = op;
		}

		@Override
		public String toString() {
			return String.format("%4d%s", mCost, mOp);
		}
	}

	private enum Transformation {
		DELETE(2, -1, 0), INSERT(
				3, 0, -1), NOOP(0, 0, 0), REPLACE(4, -1, -1), RIGHT(0, -1, -1);

		final int mCost;
		final int mDx;
		final int mDy;

		Transformation(int cost, int dx, int dy) {
			mCost = cost;
			mDx = dx;
			mDy = dy;
		}

		@Override
		public String toString() {
			switch (this) {
			case NOOP:
				return "-";
			case RIGHT:
				return ">";
			case REPLACE:
				return "R";
			case DELETE:
				return "D";
			case INSERT:
				return "I";
			default:
				throw new RuntimeException("enum not implemented");
			}
		}
	}

	private Entry[][] c;

	public int distance(String x, String y) {
		final int m = x.length();
		final int n = y.length();

		c = new Entry[x.length() + 1][y.length() + 1];
		c[0][0] = new Entry(0, Transformation.NOOP);

		for (int i = 0; i <= m; i++) {
			for (int j = 0; j <= n; j++) {
				if (i == 0 && j == 0) {
					continue;
				}

				c[i][j] = new Entry(Integer.MAX_VALUE, Transformation.NOOP);

				if (i > 0 && j > 0 && x.charAt(i - 1) == y.charAt(j - 1)) {
					updateIfCheaper(i, j, Transformation.RIGHT);
				}
				if (i > 0 && j > 0) {
					updateIfCheaper(i, j, Transformation.REPLACE);
				}
				if (i > 0) {
					updateIfCheaper(i, j, Transformation.DELETE);
				}
				if (j > 0) {
					updateIfCheaper(i, j, Transformation.INSERT);
				}
			}
		}
		return c[m][n].mCost;
	}

	String getTransformationSequence() {
		Stack<Transformation> ops = new Stack<Transformation>();
		
		int i = c.length - 1;
		int j = c[0].length - 1;

		while (i != 0 || j != 0) {
			ops.push(c[i][j].mOp);
			i += c[i][j].mOp.mDx;
			j += c[i][j].mOp.mDy;
		}
		
		StringBuffer sb = new StringBuffer();
		
		for (Transformation op : ops) {
			sb.append(op);
		}
		
		return sb.toString();
	}
	
	void printTable() {
		for (int i = 0; i < c.length; i++) {
			System.out.format("%2d:", i);
			for (int j = 0; j < c[i].length; j++) {
				System.out.print(c[i][j]);
			}
			System.out.println();
		}
	}

	private void updateIfCheaper(int i, int j, Transformation op) {
		final int otherCost = c[i + op.mDx][j + op.mDy].mCost;
		
		if (otherCost + op.mCost < c[i][j].mCost) {
			c[i][j].mCost = otherCost + op.mCost;
			c[i][j].mOp = op;
		}
	}
}
