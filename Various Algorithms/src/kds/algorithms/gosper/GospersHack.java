package kds.algorithms.gosper;

public class GospersHack {

    private static final int MAX_N = 31;
    
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("usage: java kds.algorithms.gosper.GospersHack N M");
            System.exit(1);
        }
        final int n = Integer.parseInt(args[0]);
        final int m = Integer.parseInt(args[1]);
        if (n > MAX_N) {
            System.err.println("N exceeds " + MAX_N);
            System.exit(1);
        }
        final int limit = (1 << n);
        int i = 0;
        int s = (1 << m) - 1;
        while (s < limit) {
            System.out.printf("%2d: 0x%08x\n", i, s);
            final int c = s & -s;
            final int r = s + c;
            s = (((r ^ s) >>> 2) / c) | r;
            i++;
        }
    }
}
