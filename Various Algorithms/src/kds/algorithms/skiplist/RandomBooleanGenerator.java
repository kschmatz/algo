package kds.algorithms.skiplist;

import java.util.Random;

class RandomBooleanGenerator {

	private Random mGenerator = new Random();
	private int mBits;
	private int mNumBitsAvailable;
	
	boolean next() {
		if (mNumBitsAvailable == 0) {
			mBits = mGenerator.nextInt();
			mNumBitsAvailable = 32;
		}
		boolean res = (mBits & 1) == 1;
		mBits >>= 1;
		mNumBitsAvailable--;
		return res;
	}
}
