package kds.algorithms.powering;

import java.math.BigInteger;

public class Powering {

	public static BigInteger powerRecursive(BigInteger base, int n) {
		if (n < 0) {
			throw new IllegalArgumentException("n must be non-negative");
		} else if (n == 0) {
			return BigInteger.ONE;
		} else if (n % 2 == 0) {
			BigInteger i = powerRecursive(base, n / 2);
			return i.multiply(i);
		} else {
			BigInteger i = powerRecursive(base, n / 2); // n / 2 == (n - 1) / 2
			return i.multiply(i).multiply(base);
		}
	}
	
	public static BigInteger powerIterative(BigInteger base, int n) {
		if (n < 0) {
			throw new IllegalArgumentException("n must be non-negative");
		}
		BigInteger res = BigInteger.ONE;
		BigInteger factor = base;
		for (int i = n; i > 0; i /= 2) {
			if (i % 2 != 0) {
				res = res.multiply(factor);
			}
			factor = factor.multiply(factor);
		}
		return res;
	}
}
