package kds.algorithms.queens;

public class RecursiveSolver {

	public static void main(String[] args) {
		RecursiveSolver solver = new RecursiveSolver();
		if (solver.solve(0)) {
			solver.print();
		}
	}

	private int[] board = new int[8];
	
	private boolean safe(int i) {
		final int j = board[i];
		for (int k = 1; k <= i; k++) {
			final int b = board[i - k];
			if (b == j || b == j - k || b == j + k) {
				return false;
			}
		}
		return true;
	}
	
	public void print() {
		for (int j = 0; j < board.length; j++) {
			for (int i = 0; i < board.length; i++) {
				char ch = (board[i] == j) ? 'Q' : '.';
				System.out.print(ch);
			}
			System.out.println();
		}
	}

	public boolean solve(int i) {
		final int n = board.length;
	
		if (i == n) {
			return true;
		}
		for (int j = 0; j < n; j++) {
			board[i] = j;
			if (safe(i)) {
				if (solve(i + 1)) {
					return true;
				}
			}
		}
		return false;
	}
}