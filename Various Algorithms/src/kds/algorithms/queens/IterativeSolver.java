package kds.algorithms.queens;

public class IterativeSolver {

	public static void main(String[] args) {
		IterativeSolver solver = new IterativeSolver();
		if (solver.solve()) {
			solver.print();
		}
	}

	private int[] board = new int[8];

	public void print() {
		for (int j = 0; j < board.length; j++) {
			for (int i = 0; i < board.length; i++) {
				char ch = (board[i] == j) ? 'Q' : '.';
				System.out.print(ch);
			}
			System.out.println();
		}
	}

	private boolean safe(int i) {
		final int j = board[i];
		for (int k = 1; k <= i; k++) {
			final int b = board[i - k];
			if (b == j || b == j - k || b == j + k) {
				return false;
			}
		}
		return true;
	}

	public boolean solve() {
		final int n = board.length;
		int i = 0;
		board[0] = -1;
		while (i >= 0) {
			do {
				board[i]++;
			} while (board[i] < n && !safe(i));
			if (board[i] < n) {
				if (++i < n) {
					board[i] = -1;
				} else {
					return true;
				}
			} else {
				i--;
			}
		}
		return false;
	}
}
