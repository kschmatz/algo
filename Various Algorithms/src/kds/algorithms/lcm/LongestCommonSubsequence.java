package kds.algorithms.lcm;

/*
 * Example for dynamic programming
 */
public class LongestCommonSubsequence {

	// TODO: use less space
	// TODO: produce sequence in addition to its length
	public static int length(String x, String y) {
		final int m = x.length();
		final int n = y.length();
		final int[][] c = new int[m + 1][n + 1];
		
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <=n; j++) {
				if (x.charAt(i - 1) == y.charAt(j - 1)) {
					c[i][j] = c[i - 1][j - 1] + 1;
				} else {
					c[i][j] = Math.max(c[i][j - 1], c[i - 1][j]);
				}
			}
		}
		return c[m][n];
	}
}
