package kds.algorithms.fibonacci;

public class Fibonacci {
	public static int fib(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("n must not be negative: " + n);
		} else if (n <= 1) {
			return n;
		} else {
			int f1 = 0;
			int f = 1;
			for (int i = 2; i <= n; i++) {
				final int tmp = f + f1;
				f1 = f;
				f = tmp;
			}
			return f;
		}
	}

	public static int fibDynamicProgramming(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("n must not be negative: " + n);
		} else if (n <= 1) {
			return n;
		} else {
			int[] f = new int[n + 1];
			f[0] = 0;
			f[1] = 1;
			for (int i = 2; i <= n; i++) {
				f[i] = f[i - 1] + f[i - 2];
			}
			return f[n];
		}
	}
}
