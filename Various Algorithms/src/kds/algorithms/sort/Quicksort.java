package kds.algorithms.sort;

public class Quicksort {

	public static <E extends Comparable<? super E>> void sort(E[] a) {
		sort(a, 0, a.length - 1);
	}
	
	public static <E extends Comparable<? super E>> void sort(E[] a, int p, int q) {
		if (p < q) {
			int m = partition(a, p, q);
			sort(a, p, m - 1);
			sort(a, m + 1, q);
		}
	}
	
	private static <E extends Comparable<? super E>> int partition(E[] a, int p, int q) {
		final E pivot = a[p];
		int i = p;
		for (int j = p + 1; j <= q; j++) {
			if (a[j].compareTo(pivot) <= 0) {
				E tmp = a[++i];
				a[i] = a[j];
				a[j] = tmp;
			}
		}
		a[p] = a[i];
		a[i] = pivot;
		return i;
	}
	
/*
 	private static <E extends Comparable<? super E>> int partition(E[] a, int l, int r) {
		E p = a[r];
		int i = l;
		int j = r - 1;
		while (i < j) {
			while (i < r && a[i].compareTo(p) <= 0) {
				i++;
			}
			while (j > l && a[j].compareTo(p) >= 0) {
				j--;
			}
			if (i < j) {
				E tmp = a[i];
				a[i] = a[j];
				a[j] = tmp;
			}
		}
		if (a[i].compareTo(p) > 0) {
			a[r] = a[i];
			a[i] = p;
		}
		return i;
	}
*/
	
	
}