package com.kds;

import java.util.Arrays;
import java.util.List;

class Step {
	public static final Step EAST = new Step(1, 0);
	public static final Step NORTH = new Step(0, -1);
	public static final Step SOUTH = new Step(0, 1);
	public static final Step WEST = new Step(-1, 0);

	private static final Step[] STEPS = { NORTH, SOUTH, WEST, EAST };

	public static List<Step> all() {
		return Arrays.asList(STEPS);
	}

	private final int dx;
	private final int dy;
	
	private Step(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public int dx() {
		return dx;
	}
	
	public int dy() {
		return dy;
	}
}
