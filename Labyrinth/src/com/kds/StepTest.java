package com.kds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;

public class StepTest {

	@Test
	public void testAll() {
		boolean seenEast = false;
		boolean seenNorth = false;
		boolean seenSouth = false;
		boolean seenWest = false;
		
		for (Step s : Step.all()) {
			if (s == Step.EAST) {
				assertFalse(seenEast);
				assertEquals(1, s.dx());
				assertEquals(0, s.dy());
				seenEast = true;
			} else if (s == Step.NORTH) {
				assertFalse(seenNorth);
				assertEquals(0, s.dx());
				assertEquals(-1, s.dy());
				seenNorth = true;
			} else if (s == Step.SOUTH) {
				assertFalse(seenSouth);
				assertEquals(0, s.dx());
				assertEquals(1, s.dy());
				seenSouth = true;
			} else if (s == Step.WEST) {
				assertFalse(seenWest);
				assertEquals(-1, s.dx());
				assertEquals(0, s.dy());
				seenWest = true;
			} else {
				fail("invalid Step");
			}
		}
	}

}
