package kds.algorithms.graph;

class Edge {

	public static Edge createEdge(Vertex source, Vertex sink, int capacity) {
		Edge edge = new Edge(source, sink, capacity);
		Edge redge = new Edge(sink, source, 0);
		edge.redge = redge;
		redge.redge = edge;
		return edge;
	}

	public final Vertex source;
	public final Vertex sink;

	private final int capacity;
	private int flow;
	private Edge redge;

	private Edge(Vertex source, Vertex sink, int capacity) {
		assert source != sink;
		assert capacity >= 0;
		this.source = source;
		this.sink = sink;
		this.capacity = capacity;
	}

	public void changeFlow(int diff) {
		if (flow + diff > capacity) {
			throw new IllegalArgumentException("capacity constraint violation");
		} else {
			flow += diff;
			redge.flow -= diff;
			assert redge.flow >= -capacity && redge.flow <= 0;
		}
	}

	public int flow() {
		return flow;
	}

	public Edge redge() {
		return redge;
	}

	public int residualCapacity() {
		return capacity - flow;
	}

	@Override
	public String toString() {
		return "Edge[" + source + ", " + sink + " (" + capacity + ", " + flow
				+ ")]";
	}
}
