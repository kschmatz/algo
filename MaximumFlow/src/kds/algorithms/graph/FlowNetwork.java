package kds.algorithms.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FlowNetwork {

	private Map<Vertex, List<Edge>> adj = new HashMap<Vertex, List<Edge>>();

	public void addEdge(Vertex source, Vertex sink, int capacity) {
		if (source == sink) {
			throw new IllegalArgumentException("vertices must be different");
		}
		if (!adj.containsKey(source) || !adj.containsKey(sink)) {
			throw new IllegalArgumentException("vertex is unknown");
		}
		if (capacity <= 0) {
			throw new IllegalArgumentException("capacity must be greater than 0");
		}

		Edge edge = Edge.createEdge(source, sink, capacity);
		List<Edge> vEdges = getEdges(source);
		vEdges.add(edge);
		List<Edge> wEdges = getEdges(sink);
		wEdges.add(edge.redge());
	}

	public Vertex addVertex(String id) {
		Vertex v = new Vertex(id);
		adj.put(v, new LinkedList<Edge>());
		return v;
	}

	public int calculateMaxFlow(Vertex source, Vertex sink) {
		if (source == sink) {
			throw new IllegalArgumentException("vertices must be different");
		}
		if (!adj.containsKey(source) || !adj.containsKey(sink)) {
			throw new IllegalArgumentException("vertex is unknown");
		}

		int res = 0;

		for (List<Edge> augmentingPath = findPath(source, sink); augmentingPath != null; augmentingPath = findPath(
				source, sink)) {
			int min = Integer.MAX_VALUE;
			for (Edge e : augmentingPath) {
				min = Math.min(min, e.residualCapacity());
			}
			for (Edge e : augmentingPath) {
				e.changeFlow(min);
			}
			res += min;
		}
		return res;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		for (List<Edge> list : adj.values()) {
			for (Edge e : list) {
				builder.append(e);
				builder.append('\n');
			}
		}
		return builder.toString();
	}

	private List<Edge> findPath(Vertex source, Vertex sink) {
		assert source != sink;
		
		Map<Vertex, Edge> edgeTo = new HashMap<Vertex, Edge>();

		LinkedList<Vertex> queue = new LinkedList<Vertex>();
		queue.add(source);
		edgeTo.put(source, null);

		while (!queue.isEmpty()) {
			Vertex v = queue.remove();
			for (Edge e : getEdges(v)) {
				if (e.residualCapacity() > 0) {
					Vertex w = e.sink;
					if (!edgeTo.containsKey(w)) {
						edgeTo.put(w, e);
						queue.add(w);
					}
				}
			}
		}

		if (edgeTo.containsKey(sink)) {
			LinkedList<Edge> path = new LinkedList<Edge>();
			for (Vertex v = sink; v != source;) {
				Edge e = edgeTo.get(v);
				path.add(0, e);
				v = e.source;
			}
			return path;
		} else {
			return null;
		}
	}

	private List<Edge> getEdges(Vertex v) {
		List<Edge> edges = adj.get(v);
		assert edges != null;
		return edges;
	}
}
